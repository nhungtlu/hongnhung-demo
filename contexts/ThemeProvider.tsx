import React, {PropsWithChildren, useMemo, useState} from 'react';
import COLORS from '../api/color';

export type Theme = {
  textColor: string;
  buttonColor: string;
  primaryColor: string;
};

export const defaultTheme: Theme = {
  textColor: COLORS.dark,
  buttonColor: COLORS.primary,
  primaryColor: COLORS.light,
};

type ThemeContextType = {
  theme: Theme;
  setTheme?: (theme: Theme) => void;
};

export const ThemeContext = React.createContext<ThemeContextType>({
  theme: defaultTheme,
});

export const ThemeProvider: React.FC<PropsWithChildren<React.ReactNode>> = ({
  children,
}) => {
  const [theme, setTheme] = useState<Theme>(defaultTheme);

  const themeValue = useMemo(() => ({theme, setTheme}), [theme, setTheme]);

  return (
    <ThemeContext.Provider value={themeValue}>{children}</ThemeContext.Provider>
  );
};
