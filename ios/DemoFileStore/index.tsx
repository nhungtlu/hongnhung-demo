// import React from 'react';
// import {View, Text, StyleSheet} from 'react-native';

// import firestore from '@react-native-firebase/firestore';
// import MyButton from '../components/Button';

// function DemoFileStore() {
//   const usersCollection = firestore().collection('Users');

//   console.log('usersCollection', usersCollection);

//   return (
//     <View style={styles.container}>
//       <Text style={styles.title}>DemoFileStore </Text>

//       <MyButton
//         buttonText="Add User"
//         onPress={() => {
//           firestore()
//             .collection('Users')
//             .add({
//               name: 'User 1',
//               age: 20,
//             })
//             .then(() => {
//               console.log('User added!');
//             });
//         }}
//       />

//       <MyButton
//         buttonText="Get User"
//         onPress={async () => {
//           const users = await firestore().collection('Users').get();
//           users.docs.forEach(element => {
//             console.log('User', element.data());
//           });
//         }}
//       />
//     </View>
//   );
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//     backgroundColor: '#312e38',
//   },
//   title: {
//     fontWeight: 'bold',
//     fontSize: 22,
//     color: '#fff',
//   },
// });

// export default DemoFileStore;
