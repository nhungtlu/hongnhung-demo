import React, {useContext} from 'react';
import {StyleSheet, Text, View, SafeAreaView, Alert} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/MaterialIcons';
import COLORS from '../../api/color';
import {AuthUserContext} from '../../contexts/AuthUserProvider';
import {MainStackScreenProps} from '../../stacks/Navigation';

import TabViewExample from './HomeTabView';
export const HomeScreen: React.FC<MainStackScreenProps<'Home'>> = ({}) => {
  const auth = useContext(AuthUserContext);

  return (
    <SafeAreaView style={styles.safeAreaView}>
      <View style={styles.container}>
        <View style={styles.viewHeader}>
          <Text style={styles.text}> Read news and use services </Text>
          <View style={styles.viewHeader2}>
            <Text style={styles.text}> in </Text>
            <Text style={[styles.text, styles.color]}>
              Moonrise Beach Resort
            </Text>
          </View>
        </View>
        <TouchableOpacity
          onPress={() => {
            Alert.alert('Thank you for using the hotel service');
            auth.setAuth(false);
          }}>
          <Icon name="person-outline" size={38} color={COLORS.dark} />
        </TouchableOpacity>
      </View>
      <TabViewExample />
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  safeAreaView: {
    flex: 1,
    backgroundColor: COLORS.white,
  },
  container: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
  },
  viewHeader: {
    paddingBottom: 15,
  },
  viewHeader2: {
    flexDirection: 'row',
  },
  text: {
    fontSize: 25,
    fontWeight: 'bold',
  },
  color: {
    color: COLORS.primary,
  },
  viewTitle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 20,
  },
});
