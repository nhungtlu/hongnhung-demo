import {useNavigation} from '@react-navigation/core';
import * as React from 'react';
import {useCallback, useEffect} from 'react';
import {useWindowDimensions, StyleSheet} from 'react-native';
import {TabView, SceneMap} from 'react-native-tab-view';
import {useDispatch, useSelector} from 'react-redux';
import COLORS from '../../api/color';
import {Hotel} from '../../api/services';
import {fetchNews} from '../../redux/newSlice';
import {AppDispatch, RootState} from '../../redux/store';
import {MainStackScreenProps} from '../../stacks/Navigation';
import HomeScreenList from './HomeScreenList';

//All
const AllRoute: React.FC<MainStackScreenProps<'Home'>> = () => {
  const dispatch: AppDispatch = useDispatch();
  const {hotels, isFetching} = useSelector((state: RootState) => state.new);
  const navigation = useNavigation();
  useEffect(() => {
    dispatch(fetchNews());
  }, [dispatch]);

  const onHotelItemPress = useCallback(
    (item: Hotel) => {
      navigation.navigate('HomeDetail', {id: item.id});
    },
    [navigation],
  );

  return (
    <HomeScreenList
      onPress={onHotelItemPress}
      isFetching={isFetching}
      hotels={hotels}
    />
  );
};
//New
const NewRoute: React.FC<MainStackScreenProps<'Home'>> = () => {
  const dispatch: AppDispatch = useDispatch();
  const {hotels, isFetching} = useSelector((state: RootState) => state.new);
  const navigation = useNavigation();
  useEffect(() => {
    dispatch(fetchNews());
  }, [dispatch]);

  const onHotelItemPress = useCallback(
    (item: Hotel) => {
      navigation.navigate('HomeDetail', {id: item.id});
    },
    [navigation],
  );

  return (
    <HomeScreenList
      onPress={onHotelItemPress}
      isFetching={isFetching}
      hotels={hotels}
    />
  );
};
// Service
const ServiceRoute: React.FC<MainStackScreenProps<'Home'>> = ({}) => {
  const dispatch: AppDispatch = useDispatch();
  const {hotels, isFetching} = useSelector((state: RootState) => state.new);
  const navigation = useNavigation();
  useEffect(() => {
    dispatch(fetchNews());
  }, [dispatch]);

  const onHotelItemPress = useCallback(
    (item: Hotel) => {
      navigation.navigate('HomeDetail', {id: item.id});
    },
    [navigation],
  );

  return (
    <HomeScreenList
      onPress={onHotelItemPress}
      isFetching={isFetching}
      hotels={hotels}
    />
  );
};

const renderScene = SceneMap({
  all: AllRoute,
  new: NewRoute,
  service: ServiceRoute,
});

export default function TabViewExample() {
  const layout = useWindowDimensions();

  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: 'all', title: 'All'},
    {key: 'new', title: 'New'},
    {key: 'service', title: 'Service'},
  ]);

  return (
    <TabView
      navigationState={{index, routes}}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={{width: layout.width}}
      style={styles.tabview}
    />
  );
}

export const styles = StyleSheet.create({
  tab: {
    flex: 1,
  },
  categoryListContainer: {
    flexDirection: 'row',
  },
  tabview: {
    backgroundColor: COLORS.white,
  },
  cardImage: {
    height: 150,
    marginTop: 20,
    marginBottom: 15,
    marginLeft: 10,
    marginRight: 10,
    width: '50%',
    borderRadius: 15,
  },
  cardName: {
    marginTop: 20,
    color: COLORS.primary,
    fontSize: 20,
    fontWeight: 'bold',
  },
  cardDetail: {
    marginTop: 10,
    height: 85,
    width: '70%',
    color: COLORS.dark,
    fontSize: 15,
  },
  cardPrice: {
    height: 50,
    width: 60,
    backgroundColor: COLORS.primary,
    position: 'absolute',
    zIndex: 1,
    marginTop: 20,
    marginLeft: 10,
    borderTopRightRadius: 15,
    borderBottomLeftRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  star: {
    fontSize: 10,
    color: COLORS.orange,
  },
});
